package app

import kotlinx.html.DIV
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.*

data class GameHistoryEntry(val squares: List<String>)

data class GameResult(val isFinished: Boolean, val winner: String? = null) {

    fun ifGameInProgress(handler: () -> Unit) {
        if (!isFinished) handler()
    }
}

interface GameState : RState {
    var history: MutableList<GameHistoryEntry>
    var xIsNext: Boolean
    var stepNumber: Int
}

class Game : RComponent<RProps, GameState>() {

    private val status: String = "Next player: "
    private val winningLines = arrayOf(
            arrayOf(0, 1, 2),
            arrayOf(3, 4, 5),
            arrayOf(6, 7, 8),
            arrayOf(0, 3, 6),
            arrayOf(1, 4, 7),
            arrayOf(2, 5, 8),
            arrayOf(0, 4, 8),
            arrayOf(2, 4, 6)
    )

    private val winningPredicate: (List<String>, Array<Int>) -> Boolean = { squares, line ->
        squares[line[0]] != "" && squares[line[0]] == squares[line[1]] && squares[line[1]] == squares[line[2]]
    }

    init {
        state.stepNumber = 0
        state.xIsNext = true
        state.history = mutableListOf(GameHistoryEntry(
                List(9) { "" }
        ))
    }

    override fun RBuilder.render() {
        val squares = state.history.last().squares
        val gameResult = getGameResult(squares)
        val statusToDisplay = if (gameResult.isFinished) "Winner: ${gameResult.winner}" else status + getSymbol()

        val moves = state.history
                .mapIndexed { move, _ ->
                    val message = if (move == 0) "Go to game start" else "Go to move #$move"
                    li {
                        attrs.key = move.toString()
                        button {
                            +message
                            attrs.onClickFunction = {
                                jumpTo(move)
                            }
                        }
                    }
                }

        div("game") {
            div("game-board") {
                renderBoard(squares)
            }
            div("game-info") {
                div { +statusToDisplay }
                ol { moves }
            }

        }
    }

    private fun jumpTo(step: Int) {
        setState {
            xIsNext = step % 2 == 0
            stepNumber = step
        }
    }

    private fun getGameResult(squares: List<String>): GameResult {
        return winningLines
                .firstOrNull { winningPredicate(squares, it) }
                ?.let { GameResult(true, squares[it[0]]) } ?: GameResult(false)
    }

    private fun getSymbol(): String = if (state.xIsNext) "X" else "O"

    private fun RDOMBuilder<DIV>.renderBoard(squares: List<String>) {
        board(squares) {
            getGameResult(squares)
                    .ifGameInProgress {
                        if (squares[it] == "") {
                            val newSquares = squares.toMutableList().apply { this[it] = getSymbol() }.toList()
                            val oldHistory = state.history
                            oldHistory.add(GameHistoryEntry(newSquares))

                            setState {
                                history = oldHistory
                                xIsNext = xIsNext.not()
                            }
                        }
                    }

        }
    }
}

fun RBuilder.game() = child(Game::class) {}