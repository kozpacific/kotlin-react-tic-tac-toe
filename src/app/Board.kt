package app

import kotlinx.html.DIV
import react.*
import react.dom.RDOMBuilder
import react.dom.div

interface BoardState : RState {
    var squares: Array<String>
    var xIsNext: Boolean
}

interface BoardProps: RProps {
    var squares: Array<String>
    var onSquareClick: (Int) -> Unit
}

class Board : RComponent<BoardProps, BoardState>() {

    init {
        state.squares = Array(9) { "" }
        state.xIsNext = true
    }

    override fun RBuilder.render() {
        div {
            div("board-row") {
                renderSquare(0)
                renderSquare(1)
                renderSquare(2)
            }
            div("board-row") {
                renderSquare(3)
                renderSquare(4)
                renderSquare(5)
            }
            div("board-row") {
                renderSquare(6)
                renderSquare(7)
                renderSquare(8)
            }
        }
    }

    private fun RDOMBuilder<DIV>.renderSquare(i: Int) {
        square(props.squares[i]) { props.onSquareClick(i) }
    }
}

fun RBuilder.board(squares: List<String>, handleSquareClick: (Int) -> Unit) = child(Board::class) {
    attrs.squares = squares.toTypedArray()
    attrs.onSquareClick = handleSquareClick
}