package app

import kotlinx.html.js.onClickFunction
import react.*
import react.dom.button

interface SquareProps : RProps {
    var value: String
    var onClick: () -> Unit
}

class Square : RComponent<SquareProps, RState>() {

    override fun RBuilder.render() {
        button(classes = "square") {
            +props.value
            attrs.onClickFunction = {
                props.onClick()
            }
        }
    }
}

fun RBuilder.square(value: String, handleClick: () -> Unit) = child(Square::class) {
    attrs.value = value
    attrs.onClick = handleClick
}